﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Techniqly.NflScrapR.Utils.Common
{
    public interface IGameEventSink
    {
        Task Write(IEnumerable<GameEvent> gameEvents);
    }
}