﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Techniqly.NflScrapR.Utils.Common
{
    public interface IGameEventFactory
    {
        IEnumerable<GameEvent> Create(SqlDataReader reader);
    }
}