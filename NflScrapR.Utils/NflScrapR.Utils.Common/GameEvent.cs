﻿using System;
using System.Runtime.Serialization;
using ProtoBuf;

namespace Techniqly.NflScrapR.Utils.Common
{
    [ProtoContract]
    public class GameEvent
    {
        [ProtoMember(1)]
        public DateTime EventDate { get; set; }

        [ProtoMember(2)]
        public string GameId { get; set; }

        [ProtoMember(3)]
        public byte Drive { get; set; }

        [ProtoMember(4)]
        public byte Quarter { get; set; }

        [ProtoMember(5)]
        public byte? Down { get; set; }

        [ProtoMember(6)]
        public TimeSpan? EventTime { get; set; }

        [ProtoMember(7)]
        public byte? TimeUnder { get; set; }

        [ProtoMember(8)]
        public short? TimeSeconds { get; set; }

        [ProtoMember(9)]
        public short? PlayTimeDiff { get; set; }

        [ProtoMember(10)]
        public string SideOfField { get; set; }

        [ProtoMember(11)]
        public byte? YardLine { get; set; }

        [ProtoMember(12)]
        public byte? YardLine100 { get; set; }

        [ProtoMember(13)]
        public byte YardsToGo { get; set; }

        [ProtoMember(14)]
        public sbyte YardsNet { get; set; }

        [ProtoMember(15)]
        public bool? IsGoalToGo { get; set; }

        [ProtoMember(16)]
        public bool? IsFirstDown { get; set; }

        [ProtoMember(17)]
        public string PossessionTeam { get; set; }
        
        [ProtoMember(18)]
        public string DefensiveTeam { get; set; }

        [ProtoMember(19)]
        public string PlayDescription { get; set; }

        [ProtoMember(20)]
        public bool PlayAttempted { get; set; }

        [ProtoMember(21)]
        public sbyte YardsGained { get; set; }

        [ProtoMember(22)]
        public bool IsScoringPlay { get; set; }

        [ProtoMember(23)]
        public bool IsTouchdown { get; set; }

        [ProtoMember(24)]
        public string ExtraPointResult { get; set; }

        [ProtoMember(25)]
        public string TwoPointConversionResult { get; set; }

        [ProtoMember(26)]
        public string DefenseTwoPointConversionResult { get; set; }

        [ProtoMember(27)]
        public bool IsSafety { get; set; }

        [ProtoMember(28)]
        public string PlayType { get; set; }

        [ProtoMember(29)]
        public string Passer { get; set; }

        [ProtoMember(30)]
        public bool IsPassAttempt { get; set; }

        [ProtoMember(31)]
        public string PassOutcome { get; set; }

        [ProtoMember(32)]
        public string PassLength { get; set; }

        [ProtoMember(33)]
        public string PassLocation { get; set; }

        [ProtoMember(34)]
        public bool InterceptionThrown { get; set; }

        [ProtoMember(35)]
        public string Interceptor { get; set; }

        [ProtoMember(36)]
        public string Rusher { get; set; }

        [ProtoMember(37)]
        public bool IsRushAttempt { get; set; }

        [ProtoMember(38)]
        public string RunLocation { get; set; }

        [ProtoMember(39)]
        public string RunGap { get; set; }

        [ProtoMember(40)]
        public string Receiver { get; set; }

        [ProtoMember(41)]
        public bool IsReception { get; set; }

        [ProtoMember(42)]
        public string ReturnResult { get; set; }

        [ProtoMember(43)]
        public string Returner { get; set; }

        [ProtoMember(44)]
        public string FirstTackler { get; set; }

        [ProtoMember(45)]
        public string SecondTackler { get; set; }

        [ProtoMember(46)]
        public string FieldGoalResult { get; set; }

        [ProtoMember(47)]
        public sbyte? FieldGoalDistance { get; set; }

        [ProtoMember(48)]
        public bool IsFumble { get; set; }

        [ProtoMember(49)]
        public string FumbleRecoverTeam { get; set; }

        [ProtoMember(50)]
        public string FumbleRecoverPlayer { get; set; }

        [ProtoMember(51)]
        public bool IsSack { get; set; }

        [ProtoMember(52)]
        public bool PlayChallenged { get; set; }

        [ProtoMember(53)]
        public string ChallengeResult { get; set; }

        [ProtoMember(54)]
        public bool PenaltyAccepted { get; set; }

        [ProtoMember(55)]
        public string PenaltyTeam { get; set; }

        [ProtoMember(56)]
        public string PenaltyType { get; set; }

        [ProtoMember(57)]
        public string PenaltyPlayer { get; set; }

        [ProtoMember(58)]
        public byte PenaltyYards { get; set; }

        [ProtoMember(59)]
        public byte? PossessionTeamScore { get; set; }

        [ProtoMember(60)]
        public byte? DefensiveTeamScore { get; set; }

        [ProtoMember(61)]
        public sbyte? ScoreDiff { get; set; }

        [ProtoMember(62)]
        public byte? AbsScoreDiff { get; set; }

        [ProtoMember(63)]
        public short SeasonId { get; set; }
    }
}