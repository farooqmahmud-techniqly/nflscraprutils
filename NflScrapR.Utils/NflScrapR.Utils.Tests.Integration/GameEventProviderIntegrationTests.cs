﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Jil;
using StackExchange.Redis.Extensions.Jil;

namespace Techniqly.NflScrapR.Utils.Tests.Integration
{
    [TestFixture]
    public class GameEventProviderIntegrationTests
    {
        [Test]
        public async Task ProvideAsync_ReturnsGameEvents()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
            var provider = new GameEventProvider(connectionString, new GameEventFactory());

            var events = await provider.ProvideAsync();
            events.Count().Should().Be(46129);
        }

        [Test]
        public async Task WriteEventsToFile()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
            var provider = new GameEventProvider(connectionString, new GameEventFactory());

            var events = await provider.ProvideAsync();
            events.Count().Should().Be(46129);

            var serializer = new JilSerializer(Options.ISO8601PrettyPrintCamelCase);
            var file = File.CreateText(@"c:\temp\gameEvents.json");
            var bytes = serializer.Serialize(events);
            var json = Encoding.UTF8.GetString(bytes);
            file.Write(json);
           
        }
    }
}
