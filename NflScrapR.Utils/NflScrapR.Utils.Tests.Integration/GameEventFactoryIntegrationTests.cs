﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Techniqly.NflScrapR.Utils.Common;

namespace Techniqly.NflScrapR.Utils.Tests.Integration
{
    [TestFixture]
    public class GameEventFactoryIntegrationTests
    {
        private static string _connectionString;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
        }

        [Test]
        public async Task Create_CreatesGameEventsFromDatabase()
        {
            var factory = new GameEventFactory();
            var query = "select * from dbo.nflplaybyplay2015 where GameID = '2015112204' and TimeSecs = '2871' and SideofField = 'TB'";
            IList<GameEvent> events = null;

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand(query, connection))
                {
                    using (var reader = await command.ExecuteReaderAsync(CommandBehavior.CloseConnection))
                    {
                        events = factory.Create(reader).ToList();
                    }
                        
                }
            }

            var e = events.Single();

            e.EventDate.Should().Be(new DateTime(2015, 11, 22));
            e.GameId.Should().Be("2015112204");
            e.Drive.Should().Be(6);
            e.Quarter.Should().Be(1);
            e.Down.Should().NotHaveValue();
            e.EventTime.Should().Be(new TimeSpan(0, 2, 51));
            e.TimeUnder.Should().Be(3);
            e.TimeSeconds.Should().Be(2871);
            e.PlayTimeDiff.Should().Be(0);
            e.SideOfField.Should().Be("TB");
            e.YardLine.Should().Be(35);
            e.YardLine100.Should().Be(35);
            e.YardsToGo.Should().Be(0);
            e.YardsNet.Should().Be(0);
            e.IsGoalToGo.Should().BeFalse();
            e.IsFirstDown.Should().NotHaveValue();
            e.PossessionTeam.Should().Be("PHI");
            e.DefensiveTeam.Should().Be("TB");
            e.PlayDescription.Should().NotBeNullOrWhiteSpace();
            e.PlayAttempted.Should().BeTrue();
            e.YardsGained.Should().Be(13);
            e.IsScoringPlay.Should().BeFalse();
            e.IsTouchdown.Should().BeFalse();
            e.ExtraPointResult.Should().BeNullOrWhiteSpace();
            e.TwoPointConversionResult.Should().BeNullOrWhiteSpace();
            e.DefenseTwoPointConversionResult.Should().BeNullOrWhiteSpace();
            e.IsSafety.Should().BeFalse();
            e.PlayType.Should().NotBeNullOrWhiteSpace();
            e.Passer.Should().BeNullOrWhiteSpace();
            e.IsPassAttempt.Should().BeFalse();
            e.PassOutcome.Should().BeNullOrWhiteSpace();
            e.PassLength.Should().BeNullOrWhiteSpace();
            e.PassLocation.Should().BeNullOrWhiteSpace();
            e.InterceptionThrown.Should().BeFalse();
            e.Interceptor.Should().BeNullOrWhiteSpace();
            e.Rusher.Should().BeNullOrWhiteSpace();
            e.IsRushAttempt.Should().BeFalse();
            e.RunLocation.Should().BeNullOrWhiteSpace();
            e.RunGap.Should().BeNullOrWhiteSpace();
            e.Receiver.Should().BeNullOrWhiteSpace();
            e.IsReception.Should().BeFalse();
            e.ReturnResult.Should().BeNullOrWhiteSpace();
            e.Returner.Should().NotBeNullOrWhiteSpace();
            e.FirstTackler.Should().NotBeNullOrWhiteSpace();
            e.SecondTackler.Should().BeNullOrWhiteSpace();
            e.FieldGoalResult.Should().BeNullOrWhiteSpace();
            e.FieldGoalDistance.Should().BeNull();
            e.IsFumble.Should().BeFalse();
            e.FumbleRecoverTeam.Should().BeNullOrWhiteSpace();
            e.FumbleRecoverPlayer.Should().BeNullOrWhiteSpace();
            e.IsSack.Should().BeFalse();
            e.PlayChallenged.Should().BeFalse();
            e.ChallengeResult.Should().BeNullOrWhiteSpace();
            e.PenaltyAccepted.Should().BeFalse();
            e.PenaltyTeam.Should().BeNullOrWhiteSpace();
            e.PenaltyType.Should().BeNullOrWhiteSpace();
            e.PenaltyPlayer.Should().BeNullOrWhiteSpace();
            e.PenaltyYards.Should().Be(0);
            e.PossessionTeamScore.Should().Be(7);
            e.DefensiveTeamScore.Should().Be(7);

            // FluentAssertions barfs on this comparison so using NUnit assert.
            Assert.AreEqual(0, e.ScoreDiff);
            e.AbsScoreDiff.Should().Be(0);
            e.SeasonId.Should().Be(2015);
        }
    }
}
