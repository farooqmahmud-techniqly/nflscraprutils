﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using StackExchange.Redis.Extensions.Jil;
using Techniqly.NflScrapR.Utils.Common;

namespace Techniqly.NflScrapR.Utils.Tests.Integration
{
    [TestFixture]
    public class StreamSinkJilSerializerIntegrationTests
    {
        [SetUp]
        public void SetUp()
        {
            _stream = new MemoryStream();
            _serializer = new JilSerializer();
            _sink = new StreamSink(_stream, _serializer);
        }

        private static readonly IEnumerable<GameEvent> GameEvents = new List<GameEvent>
        {
            new GameEvent {GameId = "0"},
            new GameEvent {GameId = "1"},
            new GameEvent {GameId = "2"}
        };

        private StreamSink _sink;
        private MemoryStream _stream;
        private JilSerializer _serializer;

        [Test]
        public async Task WriteCorrectlySerializesGameEvents()
        {
            using (_stream)
            {
                await _sink.Write(GameEvents);
            }

            var actualGameEvents = await _serializer.DeserializeAsync<IEnumerable<GameEvent>>(_stream.GetBuffer());

            for (var i = 0; i < GameEvents.Count(); i++)
            {
                actualGameEvents.ElementAt(i).GameId.Should().Be(GameEvents.ElementAt(i).GameId);
            }
        }

        [Test]
        public async Task WriteWritesToTheStream()
        {
            using (_stream)
            {
                await _sink.Write(GameEvents);
                var bytes = _stream.GetBuffer();
                bytes.Length.Should().BeGreaterThan(0);
            }
        }
    }
}