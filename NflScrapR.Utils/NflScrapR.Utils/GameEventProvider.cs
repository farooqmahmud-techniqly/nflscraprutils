﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Techniqly.NflScrapR.Utils.Common;

namespace Techniqly.NflScrapR.Utils
{
    public sealed class GameEventProvider : IGameEventProvider
    {
        private static readonly string _query = @"SELECT * FROM dbo.nflplaybyplay2015 (NOLOCK)";
        private readonly string _connectionString;
        private readonly IGameEventFactory _gameEventFactory;

        public GameEventProvider(
            string connectionString,
            IGameEventFactory gameEventFactory)
        {
            _connectionString = connectionString;
            _gameEventFactory = gameEventFactory;
        }

        public async Task<IEnumerable<GameEvent>> ProvideAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand(_query, connection))
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        return _gameEventFactory.Create(reader).ToList();
                    }
                }
            }
        }
    }
}