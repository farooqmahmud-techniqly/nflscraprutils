﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using StackExchange.Redis.Extensions.Core;
using Techniqly.NflScrapR.Utils.Common;

namespace Techniqly.NflScrapR.Utils
{
    public class StreamSink
    {
        private readonly ISerializer _serializer;
        private readonly Stream _targetStream;

        public StreamSink(Stream targetStream, ISerializer serializer)
        {
            _targetStream = targetStream;
            _serializer = serializer;
        }

        public async Task Write(IEnumerable<GameEvent> gameEvents)
        {
            var list = gameEvents.ToList();
            var bytes = await _serializer.SerializeAsync(list);
            await _targetStream.WriteAsync(bytes, 0, bytes.Length);
        }
    }
}