﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Techniqly.NflScrapR.Utils
{
    public static class SqlDataReaderExtensions
    {
        public static object PipelinedRead(
            this SqlDataReader reader, 
            string key,
            IEnumerable<Func<object, object>> pipelineSteps)
        {
            var o = reader[key];
            
            foreach (var pipelineStep in pipelineSteps)
            {
                o = pipelineStep(o);
            }

            return o;
        }


    }
}