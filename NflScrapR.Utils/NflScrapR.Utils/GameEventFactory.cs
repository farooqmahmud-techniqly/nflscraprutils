﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Techniqly.NflScrapR.Utils.Common;

namespace Techniqly.NflScrapR.Utils
{
    public sealed class GameEventFactory : IGameEventFactory
    {
        private readonly Func<object, object> _makeNaNull = o =>
        {
            var s = (string) o;
            return string.Compare(s, "NA", StringComparison.OrdinalIgnoreCase) == 0 ? null : o;
        };

        private readonly Func<object, object> _makeEmptyStringNull = o =>
        {
            var s = (string)o;
            return string.IsNullOrWhiteSpace(s) ? null : o;
        };

        private readonly Func<object, object> _convertToDateTime = o => DateTime.Parse((string) o);
        private readonly Func<object, object> _convertToByte = o => byte.Parse((string) o);
        private readonly Func<object, object> _convertToShort = o => short.Parse((string)o);
        private readonly Func<object, object> _convertToNullableTimespan = o =>
        {
            if (o == null)
            {
                return null;
            }

            return TimeSpan.ParseExact((string) o, @"mm\:ss", null);
        };

        private readonly Func<object, object> _convertToBool = o => ((string) o).Equals("1");
        private readonly Func<object, object> _convertToNullableBool = o => ((string) o)?.Equals("1");

        private readonly Func<object, object> _convertToNullableSbyte = o =>
        {
            if (o == null)
            {
                return null;
                
            }

            return sbyte.Parse((string) o);
        };

        private readonly Func<object, object> _convertToNullableByte = o =>
        {
            if (o == null)
            {
                return null;
            }

            return byte.Parse((string)o);
        };

        private readonly Func<object, object> _convertToNullableShort = o =>
        {
            if (o == null)
            {
                return null;

            }

            return short.Parse((string)o);
        };

        private readonly Func<object, object> _convertToSbyte = o => sbyte.Parse((string)o);

        public IEnumerable<GameEvent> Create(SqlDataReader reader)
        {
            while (reader.Read())
            {
                yield return new GameEvent
                {
                    EventDate = (DateTime)reader.PipelinedRead("Date", new[] {_convertToDateTime}),
                    GameId = (string)reader.PipelinedRead("GameID", new[] {_makeNaNull}),
                    Drive = (byte)reader.PipelinedRead("Drive", new [] {_makeNaNull, _convertToByte}),
                    Quarter = (byte)reader.PipelinedRead("qtr", new[] { _makeNaNull, _convertToByte }),
                    Down = (byte?)reader.PipelinedRead("down", new [] {_makeNaNull, _convertToNullableByte}),
                    EventTime = (TimeSpan?)reader.PipelinedRead("time", new[] { _makeNaNull, _makeEmptyStringNull, _convertToNullableTimespan }),
                    TimeUnder = (byte?)reader.PipelinedRead("TimeUnder", new[] { _makeNaNull, _convertToNullableByte }),
                    TimeSeconds =(short?) reader.PipelinedRead("TimeSecs", new[] { _makeNaNull, _convertToNullableShort }),
                    PlayTimeDiff = (short?)reader.PipelinedRead("PlayTimeDiff", new[] { _makeNaNull, _convertToNullableShort }),
                    SideOfField = (string)reader.PipelinedRead("SideofField", new[] { _makeNaNull }),
                    YardLine = (byte?)reader.PipelinedRead("yrdln", new[] { _makeNaNull, _convertToNullableByte }),
                    YardLine100 = (byte?)reader.PipelinedRead("yrdline100", new[] { _makeNaNull, _convertToNullableByte }),
                    YardsToGo = (byte)reader.PipelinedRead("ydstogo", new[] { _makeNaNull, _convertToByte }),
                    YardsNet = (sbyte)reader.PipelinedRead("ydsnet", new[] { _convertToSbyte }),
                    IsGoalToGo = (bool?)reader.PipelinedRead("GoalToGo", new []{_makeNaNull, _convertToNullableBool}),
                    IsFirstDown = (bool?)reader.PipelinedRead("FirstDown", new[] { _makeNaNull, _convertToNullableBool }),
                    PossessionTeam = (string)reader.PipelinedRead("posteam", new[] { _makeNaNull }),
                    DefensiveTeam = (string)reader.PipelinedRead("DefensiveTeam", new[] { _makeNaNull }),
                    PlayDescription = (string)reader.PipelinedRead("desc", new[] { _makeNaNull }),
                    PlayAttempted = (bool)reader.PipelinedRead("PlayAttempted", new [] {_convertToBool}),
                    YardsGained = (sbyte)reader.PipelinedRead("Yards Gained", new[] {_convertToSbyte}),
                    IsScoringPlay = (bool)reader.PipelinedRead("sp", new[] {_convertToBool}),
                    IsTouchdown = (bool)reader.PipelinedRead("Touchdown", new[] { _convertToBool }),
                    ExtraPointResult = (string)reader.PipelinedRead("ExPointResult", new[] { _makeNaNull }),
                    TwoPointConversionResult = (string)reader.PipelinedRead("TwoPointConv", new[] { _makeNaNull }),
                    DefenseTwoPointConversionResult = (string)reader.PipelinedRead("DefTwoPoint", new[] { _makeNaNull }),
                    IsSafety = (bool)reader.PipelinedRead("Safety", new[] { _convertToBool }),
                    PlayType = (string)reader.PipelinedRead("PlayType", new[] { _makeNaNull }),
                    Passer = (string)reader.PipelinedRead("Passer", new[] { _makeNaNull }),
                    IsPassAttempt = (bool)reader.PipelinedRead("PassAttempt", new[] { _convertToBool }),
                    PassOutcome = (string)reader.PipelinedRead("PassOutcome", new[] { _makeNaNull }),
                    PassLength = (string)reader.PipelinedRead("PassLength", new[] { _makeNaNull }),
                    PassLocation = (string)reader.PipelinedRead("PassLocation", new[] { _makeNaNull }),
                    InterceptionThrown = (bool)reader.PipelinedRead("InterceptionThrown", new[] { _convertToBool }),
                    Interceptor =  (string)reader.PipelinedRead("Interceptor", new[] { _makeNaNull }),
                    Rusher = (string)reader.PipelinedRead("Rusher", new[] { _makeNaNull }),
                    IsRushAttempt = (bool)reader.PipelinedRead("RushAttempt", new[] { _convertToBool }),
                    RunLocation = (string)reader.PipelinedRead("RunLocation", new[] { _makeNaNull }),
                    RunGap = (string)reader.PipelinedRead("RunGap", new[] { _makeNaNull }),
                    Receiver = (string)reader.PipelinedRead("Receiver", new[] { _makeNaNull }),
                    IsReception = (bool)reader.PipelinedRead("Reception", new[] { _convertToBool }),
                    ReturnResult = (string)reader.PipelinedRead("ReturnResult", new[] { _makeNaNull }),
                    Returner = (string)reader.PipelinedRead("Returner", new[] { _makeNaNull }),
                    FirstTackler = (string)reader.PipelinedRead("Tackler1", new[] { _makeNaNull }),
                    SecondTackler = (string)reader.PipelinedRead("Tackler2", new[] { _makeNaNull }),
                    FieldGoalResult = (string)reader.PipelinedRead("FieldGoalResult", new[] { _makeNaNull }),
                    FieldGoalDistance = (sbyte?)reader.PipelinedRead("FieldGoalDistance", new[] { _makeNaNull, _convertToNullableSbyte }),
                    IsFumble = (bool)reader.PipelinedRead("Fumble", new[] { _convertToBool }),
                    FumbleRecoverTeam = (string)reader.PipelinedRead("RecFumbTeam", new[] { _makeNaNull }),
                    FumbleRecoverPlayer = (string)reader.PipelinedRead("RecFumbPlayer", new[] { _makeNaNull }),
                    IsSack = (bool)reader.PipelinedRead("Sack", new[] { _convertToBool }),
                    PlayChallenged = (bool)reader.PipelinedRead("Challenge Replay", new[] { _convertToBool }),
                    ChallengeResult = (string)reader.PipelinedRead("ChalReplayResult", new[] { _makeNaNull }),
                    PenaltyAccepted = (bool)reader.PipelinedRead("Accepted Penalty", new[] { _convertToBool }),
                    PenaltyTeam = (string)reader.PipelinedRead("PenalizedTeam", new[] { _makeNaNull }),
                    PenaltyType = (string)reader.PipelinedRead("PenaltyType", new[] { _makeNaNull }),
                    PenaltyPlayer = (string)reader.PipelinedRead("PenalizedPlayer", new[] { _makeNaNull }),
                    PenaltyYards = (byte)reader.PipelinedRead("Penalty Yards", new[] { _convertToByte }),
                    PossessionTeamScore = (byte?)reader.PipelinedRead("PosTeamScore", new[] { _makeNaNull, _convertToNullableByte }),
                    DefensiveTeamScore = (byte?)reader.PipelinedRead("DefTeamScore", new[] { _makeNaNull, _convertToNullableByte }),
                    ScoreDiff = (sbyte?)reader.PipelinedRead("ScoreDiff", new[] { _makeNaNull, _convertToNullableSbyte }),
                    AbsScoreDiff = (byte?)reader.PipelinedRead("AbsScoreDiff", new[] { _makeNaNull, _convertToNullableByte }),
                    SeasonId = (short)reader.PipelinedRead("Season", new[] {_convertToShort})
                };
            }
        }
    }
}