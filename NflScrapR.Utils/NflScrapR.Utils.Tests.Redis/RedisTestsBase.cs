﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Jil;
using StackExchange.Redis.Extensions.Protobuf;
using Techniqly.NflScrapR.Utils.Common;

namespace Techniqly.NflScrapR.Utils.Tests.Redis
{
    [TestFixture]
    public class RedisTestsBase
    {
        private readonly ISerializer _serializer;
        private static IList<GameEvent> _gameEvents;

        public RedisTestsBase(ISerializer serializer)
        {
            _serializer = serializer;
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
            var provider = new GameEventProvider(connectionString, new GameEventFactory());
            _gameEvents = provider.ProvideAsync().Result.ToList();

        }

        [SetUp]
        public void SetUp()
        {
            var mux = ConnectionMultiplexer.Connect(new ConfigurationOptions
            {
                EndPoints = {{_host, _port}},
                AllowAdmin = true
            });

            _client = new StackExchangeRedisCacheClient(mux, _serializer);
            _db = _client.Database;

            Console.WriteLine($"Memory at test start: {GetMemoryInfo()}");
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine($"Memory at test end: {GetMemoryInfo()}");

            _db.FlushDatabase();
            _db.Multiplexer.Dispose();
            _client.Dispose();
        }

        private static readonly string _host = ConfigurationManager.AppSettings["redis:host"];
        private static readonly int _port = int.Parse(ConfigurationManager.AppSettings["redis:port"]);

        private ICacheClient _client;
        private IDatabase _db;

        [Test]
        public async Task RedisWrite_AddAllAsync()
        {
            var added = await _client.AddAllAsync(
                _gameEvents.Select(
                e => new Tuple<string, GameEvent>(
                    $"{e.GameId}:{e.TimeSeconds}:{e.PlayDescription.GetHashCode()}"
                    , e)).ToList());

            added.Should().BeTrue();
        }

        [Test]
        public async Task RedisWrite_AddAsync()
        {
            foreach (var gameEvent in _gameEvents.Take(10000))
            {
                var added = await _client.AddAsync(
                    $"{gameEvent.GameId}:{gameEvent.TimeSeconds}:{gameEvent.PlayDescription.GetHashCode()}",
                    gameEvent);

                Assert.IsTrue(added);
            }
        }

        [Test]
        public async Task Redis_AddHashSetAsync()
        {
            var props = typeof(GameEvent).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var gameEvent in _gameEvents.Take(10000))
            {
                var gameEventKey = $"{gameEvent.GameId}:{gameEvent.TimeSeconds}:{gameEvent.PlayDescription.GetHashCode()}";

                foreach (var prop in props)
                {
                    var added = await _client.HashSetAsync(
                        gameEventKey,
                        prop.Name,
                        prop.GetValue(gameEvent));

                    added.Should().BeTrue();
                }
                
            }
        }

        private string GetMemoryInfo()
        {
            var info = _client.GetInfo();
            return info["used_memory_human"];
        }
    }
}