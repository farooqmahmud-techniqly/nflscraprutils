﻿using StackExchange.Redis.Extensions.Jil;

namespace Techniqly.NflScrapR.Utils.Tests.Redis
{
    public class RedisJilSerializerTests : RedisTestsBase
    {
        public RedisJilSerializerTests() : base(new JilSerializer())
        {
            
        }
    }
}