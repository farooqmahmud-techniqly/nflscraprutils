﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace Techniqly.NflScrapR.Utils.Tests.Redis
{
    internal static class RedisDatabaseExtensions
    {
        public static void FlushDatabase(this IDatabase db)
        {
            var endPoints = db.Multiplexer.GetEndPoints();

            foreach (var endpoint in endPoints)
            {
                var server = db.Multiplexer.GetServer(endpoint);

                server.FlushDatabase();
            }
        }
    }
}
