﻿using StackExchange.Redis.Extensions.Protobuf;

namespace Techniqly.NflScrapR.Utils.Tests.Redis
{
    public class RedisProtobufSerializerTests : RedisTestsBase
    {
        public RedisProtobufSerializerTests() : base(new ProtobufSerializer())
        {
            
        }
    }
}